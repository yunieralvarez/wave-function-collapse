use rand::{seq::SliceRandom, thread_rng};
use std::f32;

#[derive(Clone)]
pub struct Tile {
    pub id: i32,
    pub right: i32,
    pub down: i32,
    pub left: i32,
    pub up: i32,
    pub weight: i32,
}

pub type Tiles = Vec<Tile>;

#[derive(Clone)]
pub struct Cell {
    pub collapsed_into_tile_id: Option<i32>,
    tile_options_ids: Vec<i32>,
    pub coord: Coord,
}

impl Cell {
    fn new(collapsed_into_tile_id: Option<i32>, coord: Coord, tile_options_ids: Vec<i32>) -> Self {
        Self {
            tile_options_ids,
            coord,
            collapsed_into_tile_id,
        }
    }

    fn is_collapsed(&self) -> bool {
        self.collapsed_into_tile_id.is_some()
    }

    fn entropy(&self, tiles: &Tiles) -> f32 {
        let cell_tile_options: Vec<Tile> = tiles
            .iter()
            .filter(|t| self.tile_options_ids.contains(&t.id))
            .cloned()
            .collect();

        calc_entropy(&cell_tile_options)
    }
}

#[derive(Clone, PartialEq)]
pub struct Coord {
    pub x: i32,
    pub y: i32,
}

impl Coord {
    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }
}

type Grid = Vec<Cell>;

pub struct CellIteration {
    pub coord: Coord,
    pub tile_id: Option<i32>,
    pub entropy: f32,
}

pub struct GridIteration {
    pub grid: Vec<CellIteration>,
    pub index: usize,
}

type GridIterations = Vec<GridIteration>;

pub fn collapse(tiles: &Tiles, x: i32, y: i32) -> GridIterations {
    let mut grid: Grid = vec![];
    let mut grid_iterations: GridIterations = vec![];

    for x_coord in 0..y {
        for y_coord in 0..x {
            grid.push(Cell::new(
                None,
                Coord::new(x_coord, y_coord),
                tiles.iter().map(|t| t.id).collect(),
            ));
        }
    }

    let mut cells_count_to_collapse = x * y;
    let mut retries = 0;

    while cells_count_to_collapse > 0 {
        if let Some(coord) =
            find_random_not_collapsed_cell_coords_with_the_least_entropy(&grid, tiles)
        {
            let cell = grid
                .iter_mut()
                .find(|cell| cell.coord.x == coord.x && cell.coord.y == coord.y)
                .unwrap();

            // cell.collapsed_into_tile_id

            // let cell_tile = tiles
            //     .iter()
            //     .find(|t| t.id == cell.collapsed_into_tile_id.unwrap())
            //     .unwrap();

            // Pick a random tile to collapse into, taking into account tiles' weight
            let mut tile_option_ids: Vec<i32> = vec![];

            for option_id in &cell.tile_options_ids {
                if let Some(tile_option) = tiles.iter().find(|t| t.id == *option_id) {
                    for _ in 0..tile_option.weight {
                        tile_option_ids.push(tile_option.id);
                    }
                }
            }

            if let Some(tile_option_id) = tile_option_ids.choose(&mut thread_rng()).copied() {
                retries = 0;
                cell.collapsed_into_tile_id = Some(tile_option_id);
                cell.tile_options_ids = vec![tile_option_id];
                cells_count_to_collapse -= 1;

                if let Some(cell_tile) = tiles
                    .iter()
                    .find(|t| t.id == cell.collapsed_into_tile_id.unwrap())
                {
                    // let cell_tile = tiles
                    //     .iter()
                    //     .find(|t| t.id == cell.collapsed_into_tile_id.unwrap())
                    //     .unwrap();

                    // Update neighboring cells' options
                    let neighbors = &[
                        (coord.x, coord.y - 1), // Up
                        (coord.x + 1, coord.y), // Right
                        (coord.x, coord.y + 1), // Down
                        (coord.x - 1, coord.y), // Left
                    ];

                    for (neighbor_x, neighbor_y) in neighbors.iter() {
                        if *neighbor_x >= 0
                            && *neighbor_x < x
                            && *neighbor_y >= 0
                            && *neighbor_y < y
                        {
                            let neighbor_coord = Coord::new(*neighbor_x, *neighbor_y);

                            let neighbor = grid
                                .iter_mut()
                                .find(|cell| {
                                    cell.coord.x == neighbor_coord.x
                                        && cell.coord.y == neighbor_coord.y
                                })
                                .unwrap();

                            if *neighbor_x == coord.x {
                                // Same column (up or down)
                                let valid_option_ids: Vec<i32> = neighbor
                                    .tile_options_ids
                                    .iter()
                                    .filter(|id| {
                                        let tile = tiles.iter().find(|t| t.id == **id).unwrap();
                                        tile.up == cell_tile.down && tile.down == cell_tile.up
                                    })
                                    .cloned()
                                    .collect();

                                neighbor.tile_options_ids = valid_option_ids;
                            }

                            if *neighbor_y == coord.y {
                                // Same row (left or right)
                                let valid_option_ids: Vec<i32> = neighbor
                                    .tile_options_ids
                                    .iter()
                                    .filter(|id| {
                                        let tile = tiles.iter().find(|t| t.id == **id).unwrap();
                                        tile.left == cell_tile.right && tile.right == cell_tile.left
                                    })
                                    .cloned()
                                    .collect();
                                neighbor.tile_options_ids = valid_option_ids;
                            }
                        }
                    }

                    let cells_iteration = grid.iter().map(|cell| CellIteration {
                        coord: Coord {
                            x: cell.coord.x,
                            y: cell.coord.y,
                        },
                        tile_id: cell.collapsed_into_tile_id,
                        entropy: calc_entropy(
                            &tiles
                                .iter()
                                .filter(|t| cell.tile_options_ids.contains(&t.id))
                                .cloned()
                                .collect(),
                        ),
                    });

                    let grid_iteration = GridIteration {
                        grid: cells_iteration.collect(),
                        index: grid_iterations.len(),
                    };

                    grid_iterations.push(grid_iteration);
                }
            } else {
                retries += 1;
                if retries == 1000 {
                    return grid_iterations;
                }

                continue;
            }
        } else {
            return grid_iterations;
        }
    }

    grid_iterations
}

fn find_random_not_collapsed_cell_coords_with_the_least_entropy(
    grid: &Grid,
    tiles: &Tiles,
) -> Option<Coord> {
    let min_entropy = grid
        .iter()
        .filter(|cell| !cell.is_collapsed())
        .map(|cell| cell.entropy(tiles))
        .fold(f32::INFINITY, f32::min);

    let all_entropy: Vec<f32> = grid
        .iter()
        .filter(|cell| !cell.is_collapsed())
        .map(|cell| cell.entropy(tiles))
        .collect();

    let min_entropy_cells: Vec<&Cell> = grid
        .iter()
        .filter(|cell| !cell.is_collapsed())
        .filter(|cell| cell.entropy(tiles) == min_entropy)
        .collect();

    let random_least_entropy_cell = min_entropy_cells.choose(&mut thread_rng());

    random_least_entropy_cell.map(|cell| cell.coord.clone())
}

fn calc_entropy(tiles: &Tiles) -> f32 {
    if tiles.is_empty() {
        return 0.0;
    }

    let res: f32 = tiles
        .iter()
        .filter(|t| t.weight as f32 > f32::EPSILON) // Avoid log(0)
        .map(|t| {
            -(t.weight as f32 / tiles.len() as f32) * (t.weight as f32 / tiles.len() as f32).log2()
        })
        .sum();

    res
}

// use rand::{seq::SliceRandom, thread_rng};
// use std::f32;

// pub struct Tile {
//     pub id: i32,
//     pub right: i32,
//     pub down: i32,
//     pub left: i32,
//     pub up: i32,
//     pub weight: i32,
// }

// pub type Tiles = Vec<Tile>;

// #[derive(Clone)]
// pub struct Cell {
//     collapsed_into_tile_id: Option<i32>,
//     tile_options_ids: Vec<i32>,
//     coord: Coord,
// }

// impl Cell {
//     fn new(collapsed_into_tile_id: Option<i32>, coord: Coord, tile_options_ids: Vec<i32>) -> Self {
//         Self {
//             tile_options_ids,
//             coord,
//             collapsed_into_tile_id,
//         }
//     }

//     fn is_collapsed(&self) -> bool {
//         if let Some(_) = self.collapsed_into_tile_id {
//             return true;
//         }

//         false
//     }

//     fn entropy(&self, tiles: &Tiles) -> f32 {
//         calc_entropy(tiles)
//     }
// }

// #[derive(Clone, PartialEq)]
// struct Coord {
//     x: i32,
//     y: i32,
// }

// impl Coord {
//     fn new(x: i32, y: i32) -> Self {
//         Self { x, y }
//     }
// }

// type Grid = Vec<Cell>;

// pub fn wave_function_collapse(tiles: &Tiles, x: i32, y: i32) -> Grid {
//     let mut grid: Grid = vec![];

//     for y_coord in 0..y {
//         for x_coord in 0..x {
//             grid.push(Cell::new(
//                 None,
//                 Coord::new(x_coord, y_coord),
//                 tiles.iter().map(|t| t.id).collect(),
//             ));
//         }
//     }

//     let mut cells_count_to_collapse = x * y;

//     while cells_count_to_collapse > 0 {
//         let cell_coord_option =
//             find_random_not_collapsed_cell_coords_with_the_least_entropy(&grid, tiles);

//         if let Some(coord) = cell_coord_option {
//             let cell = grid.iter_mut().find(|cell| cell.coord == coord).unwrap();

//             let cell_tile = tiles
//                 .iter()
//                 .find(|t| t.id == cell.collapsed_into_tile_id.unwrap())
//                 .unwrap();

//             // Pick a random tile to collapse into, taking into account tiles' weight
//             let mut tile_option_ids: Vec<i32> = vec![];

//             for option_id in &cell.tile_options_ids {
//                 if let Some(tile_option) = tiles.iter().find(|t| t.id == *option_id) {
//                     for _ in 0..tile_option.weight {
//                         tile_option_ids.push(tile_option.id);
//                     }
//                 }
//             }

//             let tile_option_id_option = tile_option_ids.choose(&mut thread_rng()).copied();

//             if let Some(tile_option_id) = tile_option_id_option {
//                 cell.collapsed_into_tile_id = Some(tile_option_id);
//                 cells_count_to_collapse -= 1;

//                 // Update neighboring cells' options
//                 let neighbors = &[
//                     (coord.x, coord.y - 1), // Up
//                     (coord.x + 1, coord.y), // Right
//                     (coord.x, coord.y + 1), // Down
//                     (coord.x - 1, coord.y), // Left
//                 ];

//                 for (neighbor_x, neighbor_y) in neighbors.iter() {
//                     if *neighbor_x >= 0 && *neighbor_x < x && *neighbor_y >= 0 && *neighbor_y < y {
//                         let neighbor = grid
//                             .iter_mut()
//                             .find(|cell| cell.coord == Coord::new(*neighbor_x, *neighbor_y))
//                             .unwrap();

//                         let neighbor_tile = tiles
//                             .iter()
//                             .find(|t| t.id == neighbor.collapsed_into_tile_id.unwrap())
//                             .unwrap();

//                         if *neighbor_x == coord.x {
//                             // Same column (up or down)
//                             let valid_option_ids: Vec<i32> = neighbor
//                                 .tile_options_ids
//                                 .iter()
//                                 .filter(|id| {
//                                     let tile = tiles.iter().find(|t| t.id == **id).unwrap();
//                                     tile.up == cell_tile.down && tile.down == cell_tile.up
//                                 })
//                                 .cloned()
//                                 .collect();

//                             neighbor.tile_options_ids = valid_option_ids;
//                         }

//                         if *neighbor_y == coord.y {
//                             // Same row (left or right)
//                             let valid_option_ids: Vec<i32> = neighbor
//                                 .tile_options_ids
//                                 .iter()
//                                 .filter(|id| {
//                                     let tile = tiles.iter().find(|t| t.id == **id).unwrap();
//                                     tile.left == cell_tile.right && tile.right == cell_tile.left
//                                 })
//                                 .cloned()
//                                 .collect();
//                             neighbor.tile_options_ids = valid_option_ids;
//                         }
//                     }
//                 }
//             }
//         }
//     }

//     grid
// }

// fn find_random_not_collapsed_cell_coords_with_the_least_entropy(
//     grid: &Grid,
//     tiles: &Tiles,
// ) -> Option<Coord> {
//     let min_entropy = grid
//         .iter()
//         .filter(|cell| !cell.is_collapsed())
//         .map(|cell| cell.entropy(tiles))
//         .fold(f32::INFINITY, f32::min);

//     let min_entropy_cells: Vec<&Cell> = grid
//         .iter()
//         .filter(|cell| !cell.is_collapsed())
//         .filter(|cell| cell.entropy(tiles) == min_entropy)
//         .collect();

//     let random_least_entropy_cell = min_entropy_cells.choose(&mut thread_rng());

//     if let Some(cell) = random_least_entropy_cell {
//         return Some(cell.coord.clone());
//     }

//     None
// }

// fn calc_entropy(tiles: &Tiles) -> f32 {
//     if tiles.len() == 0 {
//         return 0.0;
//     }

//     let res: f32 = tiles
//         .iter()
//         .filter(|t| t.weight as f32 > f32::EPSILON) // Avoid log(0)
//         .map(|t| {
//             -(t.weight as f32 / tiles.len() as f32)
//                 * (t.weight as f32 / (tiles.len() as f32)).log2()
//         })
//         .sum();

//     res
// }
